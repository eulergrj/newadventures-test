<?php

use givingWithCards\Recipient;

Route::get('/', function () {
    if (Auth::check()) {
		return redirect('/home');
	} else {
		return redirect('/login');
	}
});

Auth::routes();
Route::get('/home', [
  'uses' => 'HomeController@index',
  'as' => 'home'
]);

Route::any('/get-products', [
  'uses' => 'HomeController@getProducts',
  'as'   => 'getProducts'
]);

Route::get('/add-to-cart/{id}/{currency}', [
  'uses' => 'HomeController@addToCart',
  'as'   => 'addToCart'
]);

Route::get('/checkout', 'HomeController@checkout');

Route::get('/pay', 'HomeController@pay');
