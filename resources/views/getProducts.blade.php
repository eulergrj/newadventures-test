@foreach($products as $product)
  <li class="col-sm-4 col-xs-6">
    <img src="{{$product['thumbnail']}}" alt="{{$product['name']}} Thumbnail" width="150">
    <h3>{{$product['name']}}</h3>
    <p>{{$currency}} {{$product['price']}}</p>
    <a class="btn btn-success" href="{{route('addToCart', ['id' => $product['id'], 'currency' => $currency] )}}"><i class="fa fa-cart-plus"></i> Add to Cart</a>
  </li>
@endforeach
