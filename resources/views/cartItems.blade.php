@if (session('cartItems'))
  <li class="cart-items"></li>
@else
  <li class="text-center text-white">Cart Empty</li>
@endif
