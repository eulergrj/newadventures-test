@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading row" style="margin:0">
                  <div class="col-sm-9 col-xs-12"><h4>Product List</h4></div>
                  <div class="col-sm-3 col-xs-12 text-right">
                    <select id="currency" name="currency" class="form-control">
                      <option value="EUR">Euro (EUR)</option>
                      <option value="BRL">Reais (BRL)</option>
                      <option value="USD">Dollar (USD)</option>
                    </select>
                  </div>
                </div>
                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="products row">

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
  <script type="text/javascript">

    $(document).ready(function(){
      $("#currency").trigger('change');
    });

    $( "#currency" ).change(function(e) {
      $('.products').html('<span id="loading_icon" class="col-xs-12"><i class="fa fa-spinner fa-pulse fa-3x fa-fw"></i> </span>');
      const val = $(this).val() ? $(this).val() : 'EUR';
      $.ajax({
        type: "GET",
        url: "/get-products",
        data: "currency="+val,
        success:function(data){
            $(".products").html(data);
        }
      });
    });
  </script>
@endsection
