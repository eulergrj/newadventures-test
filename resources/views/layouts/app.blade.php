<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/style.css">
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-static-top bg-primary">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand text-white" href="{{ url('/') }}">
                      <img src="http://www.newadventures.pt/img/logo-mini.png" alt="">
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @if (Auth::guest())
                            <li><a class="text-white" href="{{ route('login') }}">Login</a></li>
                            <li><a class="text-white" href="{{ route('register') }}">Register</a></li>
                        @else
                          <li class="dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                              <i class="fa fa-shopping-cart text-white"></i>
                              <span class="badge">{{Session::has('cart') ? Session::get('cart')->totalQty : ''}}</span>
                            </a>
                            <ul class="dropdown-menu cart-items" role="menu">
                              @if (Session::has('cart'))
                                @foreach(Session::get('cart')->items as $item)
                                  <li class="cart-item">
                                    <span class="qty">{{$item['qty']}} x</span>
                                    <span class="name">{{$item['item']['name']}} - </span>
                                    <span class="price">{{$item['price']}}</span>
                                  </li>
                                @endforeach
                                <li class="cart-total">
                                  <p>Total: {{Session::get('cart')->currency . ' ' . Session::get('cart')->totalPrice}}</p>
                                  <p><a href="/checkout" class="btn btn-success">Go to Checkout</a></p>
                                </li>
                              @else
                                <li class="text-center text-white">Cart Empty</li>
                              @endif
                            </ul>
                          </li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle text-white" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu" role="menu">
                                    <li>
                                        <a href="{{ route('logout') }}" class="text-white" onclick="event.preventDefault();document.getElementById('logout-form').submit();">Logout</a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endif
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    @yield('scripts')
</body>
</html>
