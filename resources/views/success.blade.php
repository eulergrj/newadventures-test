@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading row" style="margin:0">
                  <h4>Checkout</h4>
                </div>
                <div class="panel-body">
                    <h2 class="text-success">Success!!</h2>
                    <p>Your purchase of ({{$currency . ' ' . $total}}) was successfully completed!</p>
                    <a href="/home" class="btn btn-primary">Return to home</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
