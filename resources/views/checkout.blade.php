@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading row" style="margin:0">
                  <div class="col-sm-8 col-xs-12"><h4>Product List</h4></div>
                  <div class="col-sm-4 col-xs-12 text-right" style="padding-top:10px;">
                    <a href="/home" class="text-primary">Continue Shopping</a>
                  </div>
                </div>
                <div class="panel-body">
                    <ul class="checkout-items row">
                      @if(Session::has('cart'))
                        @foreach(Session::get('cart')->items as $item)
                          <li class="checkout-item row col-sm-12">
                            <div class="col-sm-9">
                              <span class="qty">{{$item['qty']}} x </span>
                              <span class="product-thumbnail"><img src="{{$item['item']['thumbnail']}}" width="50"> </span>
                              <span class="name">{{$item['item']['name']}}</span>
                            </div>
                            <div class="col-sm-3">
                              <h4 class="price">{{$item['currency'] . ' ' . $item['price']}}</h4>
                            </div>
                          </li>
                        @endforeach
                      @endif
                      <li class="checkout-total row col-sm-12 text-right">
                        <p><a href="/pay" class="btn btn-success">Pay {{Session::get('cart')->currency . ' ' . Session::get('cart')->totalPrice}}</a></p>
                      </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
