<?php

use Illuminate\Database\Seeder;
use newAdventures\User;
use newAdventures\Product;
use newAdventures\Currency;

class DatabaseSeeder extends Seeder
{
    public function run(){
        $this->call(UsersTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
    }
}

class UsersTableSeeder extends Seeder {
    public function run(){
        DB::table('users')->delete();
        User::create([
          'name' => 'John Smith',
          'email' => 'demo@email.com',
          'password' => bcrypt('demo'),
        ]);
    }
}

class ProductsTableSeeder extends Seeder {
    public function run(){
        DB::table('products')->delete();
        $products = [
          [
            'name' => 'Product 1',
            'thumbnail' => 'http://amazingshopping.in/wp-content/uploads/2017/09/graphic_product_tangible.png',
            'price' => 9.99,
          ],[
            'name' => 'Product 2',
            'thumbnail' => 'http://amazingshopping.in/wp-content/uploads/2017/09/graphic_product_tangible.png',
            'price' => 19.99,
          ],[
            'name' => 'Product 3',
            'thumbnail' => 'http://amazingshopping.in/wp-content/uploads/2017/09/graphic_product_tangible.png',
            'price' => 29.99,
          ],[
            'name' => 'Product 4',
            'thumbnail' => 'http://amazingshopping.in/wp-content/uploads/2017/09/graphic_product_tangible.png',
            'price' => 39.99,
          ],[
            'name' => 'Product 5',
            'thumbnail' => 'http://amazingshopping.in/wp-content/uploads/2017/09/graphic_product_tangible.png',
            'price' => 49.99,
          ]
        ];
        foreach ($products as $product) {
          Product::create($product);
        }
    }
}

class CurrenciesTableSeeder extends Seeder {
    public function run(){
        DB::table('currencies')->delete();
        $currencies = [
          [
            'name' => 'Euro',
            'code' => 'EUR',
            'rate' => 1,
          ],[
            'name' => 'Reais',
            'code' => 'BRL',
            'rate' => 3.9171,
          ],[
            'name' => 'Dollar',
            'code' => 'USD',
            'rate' => 1.1806,
          ]
        ];
        foreach ($currencies as $currency) {
          Currency::create($currency);
        }
    }
}
