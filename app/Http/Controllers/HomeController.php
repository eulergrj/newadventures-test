<?php

namespace newAdventures\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use newAdventures\Currency;
use newAdventures\Product;
use newAdventures\User;
use newAdventures\Cart;
use Session;
use Mail;

class HomeController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
      return view('home');
    }

    public function parseProducts($products,$currency){
      $currencyRate = Currency::where('code',$currency)->value('rate');

      $products = array_map(function($product) use ($currencyRate){
        $product['price'] = round($product['price'] * $currencyRate, 2);
        return $product;
      },$products);

      return $products;
    }

    public function addToCart(Request $request,$id,$currency){
      // TODO: Deal with multiple currencies
      $product = Product::where('id',$id)->get()->toArray();
      $product = $this->parseProducts($product,$currency);
      $oldCart = Session::has('cart') ? Session::get('cart') : null;
      $cart = new Cart($oldCart);;
      $cart->add($product[0], $id, $currency);

      Session::put('cart',$cart);
      return redirect()->route('home');
    }

    public function getProducts(Request $request){
      $currency = $request->input('currency') ?: 'EUR';
      $products = Product::get()->toArray();

      $products = $this->parseProducts($products,$currency);

      $data = [
        'products' => $products,
        'currency' => $currency
      ];

      return view('getProducts')->with($data);
    }

    public function checkout(Request $request){
      return view('checkout');
    }

    public function pay(Request $request){
      $cart = Session::get('cart');

      // TODO: finish configuration of mailer server
      // $user = Auth::user();
      // Mail::send('emails.thankyou', ['user' => $user], function ($m) use ($user) {
      //     $m->from('newadventures@trial.com', 'New Advenures Trial');
      //     $m->to($user->email, $user->name)->subject('Thanks for your Purchase!');
      // });

      $data = [
        'total' => $cart->totalPrice,
        'currency' => $cart->currency,
      ];
      Session::forget('cart');

      return view('success')->with($data);
    }
}
