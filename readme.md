# Web Store for New Adventures Test

Web applications created with Laravel(PHP) and MySQL;

## Demo

You can see a demo of the app on [Demo](http://newadventurestrial.000webhostapp.com/);

Demo data:
```
email: demo@email.com
pass: demo
```

## install

To install a local version of the app please follow:

#### Requirements
You must have apache and MySQL configured on the machine.

#### Steps
 1 - Clone the repository;
 ```
  git clone https://eulergrj@bitbucket.org/eulergrj/newadventures-test.git
 ```

 2 - configure the .env file with your database connections;

 3 - Run
 ```
  php artisan migrate
 ```
To create the tables on your database

4 - Run
```
php artisan db:seed
```
To populate the database with demo information

5 - Run
```
php artisan serve
```
To start the server


## Authors

Euler Ribeiro - eulergrj@gmail.com
